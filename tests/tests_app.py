import sys
import os

sys.path.append(os.path.join(sys.path[0], ".."))
from src.main import app


def test_main():
    response = app.get("/")
    assert response.status_code == 200
    assert response.json() == {"key": "hello"}


def test_get_all_recipes():
    response = app.get("/recipes")
    assert response.status_code == 200
    assert response.json() == [
        {"name": "Recipe 1", "views": 10, "description": "Description 1"},
        {"name": "Recipe 2", "views": 5, "description": "Description 2"},
    ]


def test_get_recipe():
    response = app.get("/recipe/1")
    assert response.status_code == 200
    assert response.json() == {
        "name": "Recipe 1",
        "views": 11,
        "description": "Description 1",
    }

    response = app.get("/recipe/2")
    assert response.status_code == 200
    assert response.json() == {
        "name": "Recipe 2",
        "views": 6,
        "description": "Description 2",
    }
