import json

from fastapi import FastAPI
from sqlalchemy.future import select
from sqlalchemy import func
import models
from database import engine, session

app = FastAPI()


@app.on_event("startup")
async def startup():
    """
    Создает все таблицы в базе данных при запуске приложения,
    а также заполняет таблицы.
    """
    async with engine.begin() as conn:
        await conn.run_sync(models.Base.metadata.create_all)

        # Проверяем, есть ли данные в таблице Recipe
        result = await conn.scalar(select(models.Recipe))
        count = 1 if result else 0

        if count == 0:
            # Заполняем таблицу Recipe данными
            recipe_data = [
                {
                    "name": "Recipe 1",
                    "views": 10,
                    "time": 20,
                    "description": "Description 1",
                },
                {
                    "name": "Recipe 2",
                    "views": 5,
                    "time": 30,
                    "description": "Description 2",
                },
            ]
            await conn.execute(models.Recipe.__table__.insert(), recipe_data)

        # Проверяем, есть ли данные в таблице Ingredient
        result = await conn.scalar(select(models.Ingredient))
        count = 1 if result else 0

        if count == 0:
            # Заполняем таблицу Ingredient данными
            ingredient_data = [
                {"name": "Ingredient 1", "recipe_id": 1},
                {"name": "Ingredient 2", "recipe_id": 2},
            ]
            await conn.execute(models.Ingredient.__table__.insert(), ingredient_data)


@app.on_event("shutdown")
async def shutdown():
    """
    Закрывает соединение с базой данных при остановке приложения.
    """
    await session.close()
    await engine.dispose()


@app.get("/")
def main():
    """
    Возвращает приветственное сообщение.
    """
    return {"key": "hello"}


async def add_view(recipe_id: int) -> None:
    """
    Увеличивает кол-во просмотров рецепта на 1
    :param recipe_id: ID рецепта.
    """
    recipe = await session.get(models.Recipe, recipe_id)
    print(recipe)
    if recipe:
        recipe.views += 1
        await session.commit()


@app.get("/recipes")
async def get_all_recipes():
    """
    Возвращает список всех рецептов.
    """
    res = await session.execute(select(models.Recipe))
    data = [(obj.name, obj.views, obj.description) for obj in res.scalars().all()]
    print(data)
    return json.dumps(data)


@app.get("/recipe/{recipe_id}")
async def get_recipe(recipe_id: int):
    """
    Возвращает информацию о рецепте по его ID.
    :param recipe_id: ID рецепта.
    """

    stmt = select(
        models.Recipe.name, models.Recipe.views, models.Recipe.description
    ).where(models.Recipe.id == recipe_id)

    res = await session.execute(stmt)
    await add_view(recipe_id)
    return json.dumps([dict(row) for row in res.all()])
