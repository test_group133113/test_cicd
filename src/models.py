from sqlalchemy import Column, Integer, String, ForeignKey, Text
from sqlalchemy.orm import relationship
from database import Base


class Recipe(Base):
    tablename = "recipes"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    views = Column(Integer)
    time = Column(Integer)
    description = Column(Text)

    ingredients = relationship("Ingredient", back_populates="recipe")


class Ingredient(Base):
    tablename = "ingredients"

    id = Column(Integer, primary_key=True)
    name = Column(String)
    recipe_id = Column(Integer, ForeignKey("recipes.id"))

    recipe = relationship("Recipe", back_populates="ingredients")
