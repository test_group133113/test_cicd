from pydantic import BaseModel
from typing import List


class RecipesOut(BaseModel):
    name: str
    views: int
    time: int


class ingredient(BaseModel):
    name: str


class RecipeOut(BaseModel):
    name: str
    time: int
    ingredients: List[ingredient]
    description: str
